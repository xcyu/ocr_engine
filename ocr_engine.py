import flask
import numpy as np
import tensorflow as tf
from keras.models import load_model
# from keras.utils import multi_gpu_model
import os

HOST = '0.0.0.0'  # Use host 0.0.0.0 to allow access from all devices
PORT = 8080
GPU=os.getenv('GPU', False)
# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
def init():
    global model,graph,code_table,parallel_model
    # load the pre-trained Keras model
    model_file = 'ocr_model_ep12_ba2048.h5'
    model = load_model(model_file)
    code_file = 'unicode.txt'
    with open(code_file ,'r') as f:
        code_table=f.readlines()
        # print (code_table[0])
    # if GPU:
        # parallel_model = multi_gpu_model(model,gpus=2)
    graph = tf.get_default_graph()

# Cross origin support
def sendResponse(responseObj):
    response = flask.jsonify(responseObj)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', 'GET')
    response.headers.add('Access-Control-Allow-Headers', 'accept,content-type,Origin,X-Requested-With,Content-Type,access_token,Accept,Authorization,source')
    response.headers.add('Access-Control-Allow-Credentials', True)
    return response

# API for prediction
@app.route("/predict", methods=["POST"])
def predict():
    r = flask.request
    # nparr = np.fromstring(r.data, np.uint8)
    # print (r.data)
    nparr = np.fromstring(r.data, sep=',')
    inputFeature = nparr.reshape((1,64,64,1))
    with graph.as_default():
        # if GPU:
            # raw_prediction = parallel_model.predict_classes(inputFeature)[0]
        # else:
        raw_prediction = model.predict_classes(inputFeature)[0]
        prediction =  code_table[raw_prediction]
        return sendResponse({'ocr_result': chr(int(prediction[:-1], 16))})
@app.route("/predict_bin", methods=["POST"])
def predict_bin():
    r = flask.request
    # nparr = np.fromstring(r.data, np.uint8)
    # print (r.data)
    nparr = np.frombuffer(r.data,dtype='uint8')
    nparr = np.unpackbits(nparr)
    inputFeature = nparr.reshape((1,64,64,1))
    with graph.as_default():
        raw_prediction = model.predict_classes(inputFeature)[0]
        prediction =  code_table[raw_prediction]
        return sendResponse({'ocr_result': chr(int(prediction[:-1], 16))})
# if this is the main thread of execution first load the model and then start the server
if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
"please wait until server has fully started"))
    init()
    app.run(HOST, PORT, threaded=True)
